/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hospital.management.system;
import java.util.Scanner;
import java.util.LinkedList;
/**
 *
 * @author Mahmud Hasan
 */
public class RegistrationController {
    public RegistrationController(){
        System.out.println("Please select one: \n1.Receiptionist\n2.Administrative staff\n3.Doctor\n4.Back");
        Scanner input = new Scanner(System.in);
        int selection = input.nextInt();
        
        switch(selection){
            case 1: 
                registerReceiptionist();
                break;
            case 2:
                registerAdmin();
                break;
            case 3:
                registerDoctor();
                break;
            case 4:
                registerSurgeon();
                break;
            case 5:
                new HospitalManagementSystem();
                break;
            default:
                System.out.println("Inavlid selection");
                new RegistrationController();
                break;
        
        } 
        
        
    
    }
    public void registerReceiptionist(){
        LinkedList receiptionistData = new LinkedList();
        System.out.println("Please enter the informations of Administrative staff:\nName:");
        Scanner input = new Scanner(System.in);
        String name= input.nextLine();
        receiptionistData.add(name);
        System.out.println("Plese enter Surname:");
        String surname= input.nextLine();
        receiptionistData.add(surname);
        System.out.println("Please enter the Personal ID number");
        String ID = input.nextLine();
        receiptionistData.add(ID);
        System.out.println("Please enter the birthdate (day/month/year):");
        String dob = input.nextLine();
        receiptionistData.add(dob);
        System.out.println("Enter username:");
        String username = input.nextLine();
        receiptionistData.add(username);
        System.out.println("Enter password");
        String password1 = input.nextLine();
        System.out.println("Enter password again");
        String passwrod2 = input.nextLine();
        if (passwordValisation(password1, passwrod2)==true){
            receiptionistData.add(password1);
        }else {
            receiptionistData.removeAll(receiptionistData);
            System.out.println("Invalid password please enter all data again!");
            registerReceiptionist();
        
        }
        //call DAO and pass all the data to save it in database!
        // save adminData in datanase! here activation of receiptionist will be 0 and when admin activate the user only then this id is active
    }
    
    public void registerDoctor(){
        LinkedList doctorData = new LinkedList();
        System.out.println("Please enter the informations of receiptionist:\nName:");
        Scanner input = new Scanner(System.in);
        String name= input.nextLine();
        doctorData.add(name);
        System.out.println("Plese enter Surname:");
        String surname= input.nextLine();
        doctorData.add(surname);
        System.out.println("Please enter the Personal ID number");
        String ID = input.nextLine();
        doctorData.add(ID);
        System.out.println("Please enter the birthdate (day/month/year):");
        String dob = input.nextLine();
        doctorData.add(dob);
        System.out.println("Enter username:");
        String username = input.nextLine();
        doctorData.add(username);
        System.out.println("Enter password");
        String password1 = input.nextLine();
        System.out.println("Enter password again");
        String passwrod2 = input.nextLine();
        if (passwordValisation(password1, passwrod2)==true){
            doctorData.add(password1);
        }else {
            doctorData.removeAll(doctorData);
            System.out.println("Invalid password please enter all data again!");
            registerDoctor();
        
        }
        //call DAO and pass all the data to save it in database!
        //pass doctorData and save! here activation of doctor will be 0 and when admin activate the user only then this id is active
                
    }
    
     public void registerAdmin(){
        LinkedList adminData = new LinkedList();
        System.out.println("Please enter the informations of receiptionist:\nName:");
        Scanner input = new Scanner(System.in);
        String name= input.nextLine();
        adminData.add(name);
        System.out.println("Plese enter Surname:");
        String surname= input.nextLine();
        adminData.add(surname);
        System.out.println("Please enter the Personal ID number");
        String ID = input.nextLine();
        adminData.add(ID);
        System.out.println("Please enter the birthdate (day/month/year):");
        String dob = input.nextLine();
        adminData.add(dob);
        System.out.println("Enter username:");
        String username = input.nextLine();
        adminData.add(username);
        System.out.println("Enter password");
        String password1 = input.nextLine();
        System.out.println("Enter password again");
        String passwrod2 = input.nextLine();
        if (passwordValisation(password1, passwrod2)==true){
            adminData.add(password1);
        }else {
            adminData.removeAll(adminData);
            System.out.println("Invalid password please enter all data again!");
            registerAdmin();
        
        }
        //call DAO and pass all the data to save it in database!
        //pass doctorData and save! here activation of doctor will be 0 and when admin activate the user only then this id is active
                
    }
    
    public void registerSurgeon(){
        LinkedList surgeonData = new LinkedList();
        System.out.println("Please enter the informations of Surgeon:\nName:");
        Scanner input = new Scanner(System.in);
        String name= input.nextLine();
        surgeonData.add(name);
        System.out.println("Plese enter Surname:");
        String surname= input.nextLine();
        surgeonData.add(surname);
        System.out.println("Please enter the Personal ID number");
        String ID = input.nextLine();
        surgeonData.add(ID);
        System.out.println("Please enter the birthdate (day/month/year):");
        String dob = input.nextLine();
        surgeonData.add(dob);
        System.out.println("Enter username:");
        String username = input.nextLine();
        surgeonData.add(username);
        System.out.println("Enter password");
        String password1 = input.nextLine();
        System.out.println("Enter password again");
        String passwrod2 = input.nextLine();
        if (passwordValisation(password1, passwrod2)==true){
            surgeonData.add(password1);
        }else {
            surgeonData.removeAll(surgeonData);
            System.out.println("Invalid password please enter all data again!");
            registerSurgeon();
        
        }
        //call DAO and pass all the data to save it in database!
        // save adminData in datanase! here activation of surgeon will be 0 and when admin activate the user only then this id is active
    }
    
    
    
    
    public boolean passwordValisation(String pass1,String pass2){
        Boolean valid = true;
        if(pass1!=pass2){
            valid=false;
        }else valid=true;
    
    return valid;
    }
    

}
