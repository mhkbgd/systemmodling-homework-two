/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hospital.management.system;

import java.util.Scanner;

/**
 *
 * @author Mahmud Hasan
 */
public class LoginController {
    public LoginController(){
    System.out.println("Please select one: \n1.Receiptionist\n2.Administrative staff\n3.Doctor\n4.Back");
        Scanner input = new Scanner(System.in);
        int selection = input.nextInt();
        
        switch(selection){
            case 1: 
                receiptionistLogin();
                break;
            case 2:
                adminLogin();
                break;
            case 3:
                doctorLogin();
                break;
            case 4:
                new HospitalManagementSystem();
            default:
                System.out.println("Inavlid selection");
                new LoginController();
                break;
        
        } 
    
    }
    
    public void receiptionistLogin(){
        Scanner input = new Scanner(System.in);
        System.out.println("Please neter username for receiptionist");
        String username = input.nextLine();
        System.out.println("Please enter password ");
        String password = input.nextLine();
        //call loginDAO and checks if it exist then call receiptionist controller else ask for login again
        boolean userExist = true;
        if (userExist==true){
            new ReceiptionistController();
        }else{
            System.out.println("Invalid user data!");
            receiptionistLogin();
        }
    
    
    }
    
        public void doctorLogin(){
        Scanner input = new Scanner(System.in);
        System.out.println("Please neter username for doctor");
        String username = input.nextLine();
        System.out.println("Please enter password ");
        String password = input.nextLine();
        //call loginDAO and checks if it exist then call receiptionist controller else ask for login again
        boolean userExist = true;
        if (userExist==true){
            new DoctorController();
        }else{
            System.out.println("Invalid user data!");
            doctorLogin();
        }
    
    
    }
        
        
      public void adminLogin(){
        Scanner input = new Scanner(System.in);
        System.out.println("Please neter username for administrative staff");
        String username = input.nextLine();
        System.out.println("Please enter password ");
        String password = input.nextLine();
        //call loginDAO and checks if it exist then call receiptionist controller else ask for login again
        boolean userExist = true;
        if (userExist==true){
            new AdministrativeController();
        }else{
            System.out.println("Invalid user data!");
            adminLogin();
        }
    
    
    }
}
