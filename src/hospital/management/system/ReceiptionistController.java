/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hospital.management.system;

import java.util.Scanner;
import java.util.LinkedList;

/**
 *
 * @author Mahmud Hasan
 */
public class ReceiptionistController {
    public ReceiptionistController(){
        System.out.println("Please select an action for receiptionist!\n1.Add new patient\n2.Add oncologist\n3.Logout");
        Scanner input = new Scanner(System.in);
        int selection = input.nextInt();
        switch(selection){
            case 1:
                addPatient();
                break;
            case 2:
                assignOncologist();
                break;
            case 3:
                new LoginController();
                break;
        
        }
    
    }
    
    public void addPatient(){
        LinkedList patientData = new LinkedList();
        Scanner input = new Scanner(System.in);
        System.out.println("Please enter patient informationL\n1.Patient Name:");
        String name= input.nextLine();
        patientData.add(name);
        System.out.println("Plese enter Surname:");
        String surname= input.nextLine();
        patientData.add(surname);
        System.out.println("Please enter the Personal ID number");
        String ID = input.nextLine();
        patientData.add(ID);
        System.out.println("Please enter the birthdate (day/month/year):");
        String dob = input.nextLine();
        patientData.add(dob);
        System.out.println("Enter insurance code:");
        String insuranceCode = input.nextLine();
        patientData.add(insuranceCode);
        // pass patient data to PatientDAO and save in databse
    
    }
    
    public void assignOncologist(){
        Scanner input = new Scanner(System.in);
        System.out.println("Please slelect a patient:");
        System.out.println(seePatinetList());
        int patientSelection =  input.nextInt();
        System.out.println("Please select a oncologist:");
        System.out.println(seeOncologistList());
        int oncologistSelection = input.nextInt();
        //now call receiptionist DAO and update all patient table with ncologist ID 
    }
    
    
    public LinkedList seePatinetList(){
    //access patient database and retrive all the patient data and show it as a list
    //below a demo
        LinkedList patientList = new LinkedList();
        patientList.add("1.Mahmud");
        patientList.add("2.Hasan");
        return patientList;
    }
    
    public LinkedList seeOncologistList(){
    //access doctor database and load all the doctor who has 1 in oncologist column ans show to receiptionist
        LinkedList oncologistList = new LinkedList();
        oncologistList.add("1. One oncologist");
        oncologistList.add("2. Second oncologist");
        return oncologistList;
    }
}
