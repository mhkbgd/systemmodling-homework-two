/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hospital.management.system;

import java.util.Scanner;

/**
 *
 * @author Mahmud Hasan
 */
public class HospitalManagementSystem {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        System.out.println("Welcome to the hospital management system!");
        System.out.println("Please slecet and option!\n1. Register in the system \n2. Login ");
        Scanner input = new Scanner(System.in);
        int selection =  input.nextInt();
        switch (selection){
            case 1: new RegistrationController();
                break; 
            case 2:
                new LoginController();
                break;
            default:System.out.println("Invalid selection");
                new HospitalManagementSystem();
                break;
                
        }
    }
    
}
